<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//CATEGORIES CONTROLLER
Route::post('/categories/store',    'CategoriesController@store') -> name('categories.store');

//NEWS CONTROLLER
Route::get('/news',                 'NewsController@index')       -> name('news.index');
Route::post('/news/store',          'NewsController@store')       -> name('news.store');