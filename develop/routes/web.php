<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',            'PagesController@index')             -> name('pages.index');
//NEWS
Route::get('/admin/news/create', 'AdminNewsController@create')  -> name('admin.news.create');
Route::post('/admin/news/store', 'AdminNewsController@store')   -> name('admin.news.store');
//CATEGORIES
Route::get('/admin/categories/create', 'AdminCategoriesController@create')  -> name('admin.categories.create');
Route::post('/admin/categories/store', 'AdminCategoriesController@store')   -> name('admin.categories.store');