@extends('main')

@section('stylesheets')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
@endsection

@section('content')
    <div class="container">
        {{--SUCCESS AND ERRORS HANDLING--}}
        @if(Session::has('success'))
            <div class="container margin-top-30">
                <div class="alert alert-success" role="alert">
                    <strong>Success : </strong> {!! Session::get('success') !!}
                </div>
            </div>
        @endif
        @if(count($errors) > 0)
            <div class="container margin-top-30">
                <div class="alert alert-danger" role="alert">
                    <strong>Errors:</strong>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>
                    Add A New News
                    <a href="{{ route('pages.index') }}" class="btn btn-primary pull-right btn-lg">View All News</a>
                    <a href="{{ route('admin.categories.create') }}" style="margin-right: 10px" class="btn btn-info pull-right btn-lg">Add Category</a>
                </h1>

            </div>
            <form action="{{ route('admin.news.store') }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="panel-body">
                    <label for="name">Title</label>
                    <input type="text" name="name" id="name" class="form-control">
                    <br>
                    <label for="photo">Photo</label>
                    <input type="file" name="photo" id="photo" class="form-control" accept="image/*">
                    <br>
                    <label for="category">category</label>
                    <select name="category" id="category" class="form-control">
                        <option value="">-SELECT CATEGORY-</option>
                        @foreach($categories as $category)
                            <option value="{{ $category -> id }}">{{ $category -> name }}</option>
                        @endforeach
                    </select>
                    <br>
                    <label for="labels">Labels</label>
                    <input type="text" name="labels[]" id="labels" data-role="tagsinput">
                    <br>
                    <label for="text">Description</label>
                    <textarea name="text" id="text" class="form-control"></textarea>
                    <br>
                </div>

                <div class="panel-footer">
                    <button type="submit" class="btn btn-success btn-lg btn-block">Add News</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $("#labels").tagsinput('items')
    </script>
@endsection