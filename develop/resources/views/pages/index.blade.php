@extends('main')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>
                    All News With Pagination
                    <a href="{{ route('admin.news.create') }}" class="btn btn-primary pull-right btn-lg">Add New News</a>
                </h1>

            </div>

            <div class="panel-body">
                <ul>
                    @foreach($news as $n)
                        <li style="margin-top: 50px">
                            <img src="{{ url('/') }}/uploaded_photos/{{ $n->photo }}" alt="">
                            <br>
                            <h1>{{ $n -> name }}</h1>
                            <p>{!! $n -> text !!}</p>
                            <p>{{ $n -> category -> name }}</p>

                            @foreach($n -> labels as $label)
                                <div class="label label-default" style="margin-right: 5px">{{ $label -> name }}</div>
                            @endforeach
                        </li>
                        <hr>
                    @endforeach
                </ul>
            </div>

            <div class="panel-footer">
                {{ $news -> links() }}
            </div>
        </div>
    </div>
@endsection