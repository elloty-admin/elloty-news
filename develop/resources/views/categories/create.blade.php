@extends('main')

@section('stylesheets')
@endsection

@section('content')
    <div class="container">
        {{--SUCCESS AND ERRORS HANDLING--}}
        @if(Session::has('success'))
            <div class="container margin-top-30">
                <div class="alert alert-success" role="alert">
                    <strong>Success : </strong> {!! Session::get('success') !!}
                </div>
            </div>
        @endif
        @if(count($errors) > 0)
            <div class="container margin-top-30">
                <div class="alert alert-danger" role="alert">
                    <strong>Errors:</strong>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>
                    Add A New News
                    <a href="{{ route('pages.index') }}" class="btn btn-primary pull-right btn-lg">View All News</a>
                    <a href="{{ route('pages.index') }}" style="margin-right: 10px" class="btn btn-info pull-right btn-lg">Add Category</a>
                </h1>

            </div>
            <form action="{{ route('admin.categories.store') }}" method="POST">
                {{ csrf_field() }}
                <div class="panel-body">
                    <label for="name">Category Name:</label>
                    <input type="text" name="name" id="name" class="form-control">
                    <br>
                </div>

                <div class="panel-footer">
                    <button type="submit" class="btn btn-success btn-lg btn-block">Add Category</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $("#labels").tagsinput('items')
    </script>
@endsection