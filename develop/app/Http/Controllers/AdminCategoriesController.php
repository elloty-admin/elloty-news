<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Session;

class AdminCategoriesController extends Controller
{
    public function create() {
        return view('categories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required:max:255|unique:categories',
        ]);

        $category = new Category;
        $category -> name = $request -> name;
        $category -> save();

        Session::flash('success', 'This category has been added successfully!');
        return redirect() -> back();
    }
}
