<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Session;
use App\News;
use App\Label;

class AdminNewsController extends Controller
{
    public function create() {
        $categories = Category::all();
        return view('news.create') -> withCategories($categories);
    }

    public function store(Request $request) {
        $this -> validate($request, [
            'name' => 'required:max:255',
            'photo'=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'text' => 'required',
            'category' => 'numeric|required',
            'labels' => 'required',
        ]);

        //GET UPLOADED PHOTO AND STORING IT
        $photo = $request -> file('photo');

        $photo_ori_name = $photo -> getClientOriginalName();
        $photo_gen_name = md5($photo_ori_name . microtime() . time()).$photo_ori_name;
        $photo -> move(public_path().'/uploaded_photos/', $photo_gen_name);


        //STORING THE POST
        $news = new News;
        $news -> name           = $request -> name;
        $news -> photo          = $photo_gen_name;
        $news -> text           = $request -> text;
        $news -> category_id    = $request -> category;
        $news -> save();

        $labels = explode(',', $request -> labels[0]);
        //STORING THE LABELS
        for($x = 0; $x < count($labels); $x++) {
            $label = new Label;
            $label -> news_id = $news -> id;
            $label -> name    = $labels[$x];
            $label -> save();
        }

        Session::flash('success', 'This news has been added successfully!');
        return redirect() -> back();
    }
}
