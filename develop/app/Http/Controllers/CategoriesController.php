<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Validator;


class CategoriesController extends Controller
{
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:categories'
        ]);

        if ($validator->fails()) {
            return [
                'data'      => $validator->messages(),
                'message'   => 'error',
                'status'    => '409'
            ];
        }

        $category = new Category;
        $category -> name = $request -> name;
        $category -> save();

        return [
            'data'    => $category,
            'message' => 'success',
            'status'  => '200'
        ];
    }
}
