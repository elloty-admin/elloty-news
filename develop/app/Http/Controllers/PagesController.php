<?php

namespace App\Http\Controllers;

use App\News;

class PagesController extends Controller
{
    public function index() {
        $news = News::paginate(10);
        return view('pages.index') -> withNews($news);
    }
}
