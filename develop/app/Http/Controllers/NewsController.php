<?php

namespace App\Http\Controllers;

use App\Label;
use App\News;
use Illuminate\Http\Request;
use Validator;

class NewsController extends Controller
{
    public function index() {
        $news = News::paginate(10);
        return [
            'data'      => $news,
            'message'   => 'success',
            'status'    => '200'
        ];
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required:max:255',
            'photo'=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'text' => 'required',
            'category_id' => 'numeric|required',
            'labels' => 'required',
        ]);

        //VALIDATION CONFIRM
        if ($validator->fails()) {
            return [
                'data'      => $validator->messages(),
                'message'   => 'error',
                'status'    => '409'
            ];
        }


        //GET UPLOADED PHOTO AND STORING IT
        $photo = $request -> file('photo');

        $photo_ori_name = $photo -> getClientOriginalName();
        $photo_gen_name = md5($photo_ori_name . microtime() . time()).$photo_ori_name;
        $photo -> move(public_path().'/uploaded_photos/', $photo_gen_name);


        //STORING THE POST
        $news = new News;
        $news -> name           = $request -> name;
        $news -> photo          = $photo_gen_name;
        $news -> text           = $request -> text;
        $news -> category_id    = $request -> category_id;
        $news -> save();

        //STORING THE LABELS
        for($x = 0; $x < count($request -> labels); $x++) {
            $label = new Label;
            $label -> news_id = $news -> id;
            $label -> name    = $request -> labels[$x];
            $label -> save();
        }

        return [
            'data'    => $news,
            'message' => 'success',
            'status'  => '200'
        ];
    }
}
