<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public function labels()
    {
        return $this->hasMany('App\Label', 'news_id');
    }

    public function Category()
    {
        return $this->belongsTo('App\Category');
    }
}
